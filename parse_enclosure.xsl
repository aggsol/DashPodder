<?xml version="1.0"?>
<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform"
	xmlns:media="http://search.yahoo.com/mrss/" exclude-result-prefixes="media">
    <output method="text"/>
    <template match="/">
        <for-each select = "rss/channel/item/enclosure">
            <value-of select="@url"/>
            <text>&#10;</text>
        </for-each>
        <for-each select = "//media:content">
            <value-of select="@url"/>
            <text>&#10;</text>
        </for-each>
    </template>
</stylesheet>