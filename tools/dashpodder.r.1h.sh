#!/usr/bin/env bash
set -euo pipefail
#
# Add DashPodder to the GNOME Shell via Argos (https://github.com/p-e-w/argos)
#
# START USER CONFIGURATION
# Location of DashPodder and related files are located
readonly basedir="$HOME/dashpodder"
#readonly basedir="$HOME/workspace/podcasts"

# Location of the dashpodder.sh script
readonly DASHPODDER="$basedir/dashpodder.sh"
#readonly DASHPODDER="$HOME/workspace/DashPodder/dashpodder.sh"
# END USER CONFIGURATION

# Location of the logfile wiht the latest podcasts
readonly latestlog="$basedir/latest.log"

# Directory for caching files
readonly cacheDir="$HOME/.cache/dashpodder"

# Self check files
if [[ ! -d "$basedir" ]] || [[ ! -f "$DASHPODDER" ]];
then
    echo "DashPodder | iconName=sync-error color=#ff0000"
    echo "Error: Check user configuration! | length=16"
    echo "---"
    echo "Edit user configuration | terminal=false bash='xdg-open $0' iconName=accessories-text-editor"
    exit 0
fi

# Timestamp of last automatic sync run is stored in a file
readonly timestampFile="$cacheDir/lastcheck"
if [[ ! -d "cacheDir" ]]; then
    mkdir -p "$cacheDir"
fi

# Notify on new podcasts
if [[ ! -f "$latestlog" ]] || [[ ! -s "$latestlog" ]]; then
    echo "DashPodder | iconName=applications-multimedia"
else
    echo "DashPodder | iconName=starred"
    echo "---"
    while read -r entry; do

        folder=${entry%/*} # trim after last slash
        folder=${folder##*/} # remove before last slash
        folder=${folder// /_}

        filename=$(basename "$entry")

        echo "NEW: $folder: $filename | bash='xdg-open ${entry}' iconName=media-playback-start terminal=false"

    done < "$latestlog"
fi

echo "---"
echo "Sync podcasts | terminal=true bash='$DASHPODDER -v; touch $0' iconName=view-refresh"
echo "Browse podcasts | terminal=false bash='nautilus $basedir' iconName=system-file-manager"
echo "Mark all as played | terminal=false bash='truncate -s 0 $latestlog; touch $0' iconName=mail-mark-read"
echo "Edit subscriptions | terminal=false bash='xdg-open $basedir/subscriptions.conf' iconName=accessories-text-editor"
echo "Edit configuration | terminal=false bash='xdg-open $DASHPODDER' iconName=preferences-other"
echo "Check for update | href='https://github.com/aggsol/DashPodder/releases' iconName=applications-internet"
