# Changelog

### v1.1.2 (2018-04-23)
- Sanitize filenames
- Improved Argos plugin error handling
- Improved filename detection and error handling
- Fix shellcheck warnings

### v1.1.1 (2018-03-02)
- Fix filenames from Cloudfront CDN
- Fix regression of scientific american podcasts
- Fix properly filter non-audio files
- Fix argos plugin display glitch
- Remove auto sync on GNOME Shell start (very buggy)

### v1.1.0

- Removed argos parameter
- Added latest.log for latest podcasts
- Improved Argos plugin (GNOME shell)
- Parse Media RSS
- Fixed license
- Fixed #2 (Select correct filename from cloudfront URLs)

### v1.0.0 (2018-02-09)

- Removed M3U playlist creation
- Additional sanity checks
- Rewritten argument parsing
- Fixed several warnings
- Intial fork, renamed