#!/usr/bin/env bash
set -uo pipefail

# DashPodder
# https://github.com/aggsol/DashPodder
# Copyright (c) 2018 Kim HOANG <foss@aggsol.de>
#
# Based on Mashpodder
# https://github.com/chessgriffin/mashpodder
# Copyright (c) 2009-2014 Chess Griffin <chess.griffin@gmail.com>
#
# Originally based on BashPodder
# Copyright /c) 2004 Linc Fessenden
# http://lincgeek.org/bashpodder/
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### START USER CONFIGURATION
# Default values can be set here. Command-line flags can override some of these but not all of them.

# BASEDIR: Base location of the script and related files. Default is "$HOME/dashpodder".
# Uncomment and set to desired path. DashPodder will not create this directory
#readonly BASEDIR="$HOME/dashpodder"
readonly BASEDIR="$HOME/workspace/podcasts"

# RSSFILE: Location of subscriptions.conf file. Can be changed to another filename.
# Default is "$BASEDIR/subscriptions.conf".
RSSFILE="$BASEDIR/subscriptions.conf"

# PODCASTDIR: Location of podcast directories listed in $RSSFILE. Default is "$BASEDIR".
# DashPodder will create this directory if it does not exist unless $CREATE_PODCASTDIR is set to "".
PODCASTDIR="$BASEDIR"

# CREATE_PODCASTDIR: Default "1" will create the directory for you if it does not exist; "" means to
# fail and exit if $PODCASTDIR does not exist. If your podcast directory is on a mounted share
# (e.g. NFS, Samba), then setting this to "" and thus fail is a means of detecting an unmounted
# share, and to avoid unintentionally writing to the mount point.  (This assumes that $PODCASTDIR is
# below, and not, the mount point.)
CREATE_PODCASTDIR="1"

# DATEFILEDIR: Location of the "date" directory below $PODCASTDIR Note: do not use a leading slash,
# it will get added later. The eventual location will be $PODCASTDIR/$DATEFILEDIR/$(date +$DATESTRING)
# Dashpodder will create this directory if it does not exist. Default is "", which results in date
# directories being put in $PODCASTDIR.
DATEFILEDIR=""

# TMPDIR: Location of temp logs, where files are temporarily downloaded to, and other bits.
# Dashpodder will create this directory if it does not exist but it will not be deleted on exit.
TMPDIR="/tmp/dashpodder"

# DATESTRING: Valid date format for date-based archiving. Can be changed to other valid formats.
# See man date for details.
DATESTRING="%Y-%m-%d"

# PARSE_ENCLOSURE: Location of parse_enclosure.xsl file. Default is "$BASEDIR/parse_enclosure.xsl".
PARSE_ENCLOSURE="$BASEDIR/parse_enclosure.xsl"

# PODLOG: This is a critical file.  This is the file that saves the name of every file downloaded
# (or checked with the 'update' option in subscriptions.conf.) If you lose this file then DashPodder
# should be able to automatically recreate it during the next run, but it's still a good idea to
# make sure the file is kept in a safe place.  Default is "$BASEDIR/podcast.log".
PODLOG="$BASEDIR/podcast.log"

# PODLOG_BACKUP: Setting this option to "1" will create a date-stamped backup of your podcast.log
# file before new podcast files are downloaded. The filename will be $PODLOG.$DATESTRING (see above
# variables). If you enable this, you'll want to monitor the number of backups and manually
# remove old copies. Default is "".
PODLOG_BACKUP="1"

# FIRST_ONLY: Default "" means look to subscriptions.conf for whether to download or
# update; "1" will override subscriptions.conf and download the newest episode.
FIRST_ONLY=""

# UPDATE: Default "" means look to subscriptions.conf on whether to download or
# update; "1" will override subscriptions.conf and cause all feeds to be updated
# (meaning episodes will be marked as downloaded but not actually
# downloaded).
UPDATE=""

# WGET_TIMEOUT: Default is 30 seconds; can decrease or increase if some
# files are cut short. Thanks to Phil Smith for the bug report.
WGET_TIMEOUT="30"

# Location of binaries.  Below are the paths to third-party binaries used by DashPodder. This is
# here for BSD users where these binaries are usually in /usr/local/bin. Defaults are Linux
# locations (i.e. /usr/bin).
WGET=${WGET:-"/usr/bin/wget"}
CURL=${CURL:-"/usr/bin/curl"}
XSLTPROC=${XSLTPROC:-"/usr/bin/xsltproc"}

### END USER CONFIGURATION
### No changes should be necessary below this line

readonly VERSION="v1.1.3-alpha"
readonly SCRIPT=${0##*/}
readonly CWD=$(pwd)
readonly TEMPLOG="$TMPDIR/temp.log"
readonly SUMMARYLOG="$TMPDIR/summary.log"
readonly TEMPRSSFILE="$TMPDIR/subscriptions.conf.temp" # actual subscriptions used
readonly LATESTLOG="$TMPDIR/latest.log"
readonly OLDIFS=$IFS
readonly TIMESTAMP=$(date "+%Y-%m-%d %H:%M:%S")

declare -a AUDIO_EXT=("mp3" "ogg" "oga" "m4a" "aac" "mp4" "m4p" "m4r" "3gp")
WGET_QUIET="-q"
VERBOSE=false
IFS=$'\n'
SHOW_DEBUG=true

function debug() {
    if $SHOW_DEBUG; then
        echo -e "DEBUG: $*" | tr -s ' ' | fmt -100
    fi
}

function info() {
    if $VERBOSE; then
        echo -e "$@" | tr -s ' ' | fmt -100
    fi
}

function error() {
    echo -e "Error: $*" | tr -s ' ' | fmt -100 1>&2
}

# Perform some basic checks
function sanity_checks() {

    local FEED ARCHIVETYPE DLNUM DATADIR NEWPODLOG

    info "${TIMESTAMP} Starting DashPodder..."

    if [ -z "$BASEDIR" ]; then
        error "\$BASEDIR has not been set. Please review the USER CONFIGURATION section at the \
              top of dashpodder.sh and set \$BASEDIR and any other applicable options."
        exit 101
    fi

    if [ ! -e "$BASEDIR" ]; then
        error "\$BASEDIR does not exist. Please re-check the settings at the top of dashpodder.sh and try again."
        exit 102
    fi

    cd "$BASEDIR" || exit 103

    if [[ ! -f "$PARSE_ENCLOSURE" ]]; then
        error "\$PARSE_ENCLOSURE does not exist. Please re-check settings at the top of $SCRIPT and try again."
        exit 103
    fi

    # Make podcast directory if necessary
    if [ ! -e "$PODCASTDIR" ]; then
        if [ "$CREATE_PODCASTDIR" = "1" ]; then
            if $VERBOSE; then
                echo "Creating podcast directory $PODCASTDIR"
            fi
            mkdir -p "$PODCASTDIR"
        else
            error "\$PODCASTDIR does not exist. Please re-check the settings at the top of \
                  dashpodder.sh and try again. This could also indiciate an unmounted share, \
                  if it is on a shared directory."
            exit 104
        fi
    fi

    rm -rf "$TMPDIR"
    mkdir -p "$TMPDIR"

    rm -f "$TEMPRSSFILE"
    touch "$TEMPRSSFILE"

    # Make sure the subscriptions.conf file or the file passed with -c switch exists
    if [ ! -e "$RSSFILE" ]; then
        error "The file $RSSFILE cannot be found. Run $0 -h \
              for usage and check the settings at the top of dashpodder.sh"
        exit 105
    fi

    # Check the subscriptions.conf and do some basic error checking
    LINE_COUNT=1
    while read -r LINE; do
        DLNUM="none"
        FEED=$(echo "$LINE" | cut -f1 -d ' ')

        # Skip blank lines and lines beginning with '#'
        if echo "$LINE" | grep -E '^#|^$' > /dev/null
                then
                continue
        fi

        # Feed URLs must not have trailing slashes
        if [[ "$FEED" == */ ]]; then
            echo "$RSSFILE Feed ${LINE_COUNT}: Feed URLs must not have trailing slashes."
            exit 106
        fi

        ARCHIVETYPE=$(echo "$LINE" | cut -f2 -d ' ')

        if [[ "$ARCHIVETYPE" == "$FEED" ]]; then
            echo "$RSSFILE Line ${LINE_COUNT}: Field 2 is missing."
            exit 107
        fi

        DLNUM=$(echo "$LINE" | cut -f3 -d ' ')

        if [[ "$DLNUM" != "none" && "$DLNUM" != "all" && \
            "$DLNUM" != "update" && $DLNUM -lt 1 ]]; then
            error "$RSSFILE Line ${LINE_COUNT}: Invalid Field 3. \
                It should be set to 'none', 'all', 'update', or a number \
                greater than zero."
            exit 108
        fi

        # Check type of archiving for each feed
        if [ "$DLNUM" = "update" ]; then
            DATADIR=$ARCHIVETYPE
        else
            if [ ! "$ARCHIVETYPE" = "date" ]; then
                DATADIR=$ARCHIVETYPE
            elif [ "$ARCHIVETYPE" = "date" ]; then
                if [ -n "$DATEFILEDIR" ]; then
                    DATADIR="$DATEFILEDIR/$(date +$DATESTRING)"
                else
                    DATADIR=$(date +$DATESTRING)
                fi
            else
                error "Error in archive type for $FEED.  It should be set to 'date' for date-based \
                      archiving, or to a directory name for directory-based archiving."
                exit 108
            fi
        fi

        if [ "$FIRST_ONLY" = "1" ]; then
            DLNUM="1"
        fi
        if [ "$UPDATE" = "1" ]; then
            DLNUM="update"
        fi
        echo "$FEED $DATADIR $DLNUM" >> "$TEMPRSSFILE"
        ((LINE_COUNT=LINE_COUNT+1))
    done < "$RSSFILE"

    # Backup the $PODLOG if $PODLOG_BACKUP=1
    if [ "$PODLOG_BACKUP" = "1" ]; then
        if $VERBOSE; then
            echo "Backing up the $PODLOG file"
        fi
        NEWPODLOG="$PODLOG.$(date +$DATESTRING)"
        cp "$PODLOG" "$NEWPODLOG"
    fi

    # Fresh temp log
    rm -f "$TEMPLOG"
    touch "$TEMPLOG"

    # Create podcast log if necessary
    if [ ! -e "$PODLOG" ]; then
        info "Creating $PODLOG file"
        touch "$PODLOG"
    fi

    # create empty latest log
    rm -f "$LATESTLOG"
    touch "$LATESTLOG"
}

# Taken from https://gist.github.com/cdown/1163649
function urldecode() {
    local url_encoded="${1//+/ }"
    printf '%b' "${url_encoded//%/\\x}"
}

# Check if filename has an audio extension
function isAudio() {
    local FILENAME_EXT=${1: -3}

    for EXT in "${AUDIO_EXT[@]}"
    do
        if [[ "$EXT" == "$FILENAME_EXT" ]];
        then
            echo "yes"
            return
        fi
    done
    echo "no"
}

# fix_url now based on from https://github.com/chessgriffin/mashpodder/pull/7
# Take a url embedded in a feed, get the filename, and perform some  fixes
function fix_url() {

    local FIXURL IS_AUDIO SUGGESTION FOLDER_NAME FIRSTFILENAME
    FIXURL=$(urldecode "$1")
    #debug "FIXURL=$FIXURL"
    SUGGESTION=$2
    #debug "SUGGESTION = $SUGGESTION"

    # Get the name of the folder
    FOLDER_NAME=${FIXURL%/*} # trim after last slash
    FOLDER_NAME=${FOLDER_NAME##*/} # remove before last slash
    FOLDER_NAME=${FOLDER_NAME// /_}
    #debug "FOLDER_NAME = $FOLDER_NAME"

    # Get the filename
    FIRSTFILENAME=${FIXURL##*/}
    #debug "FIRSTFILENAME = $FIRSTFILENAME"
    FILENAME=${FIRSTFILENAME%\?*}

    # Fix Podshow.com numbers that keep changing
    FILENAME=${FILENAME/_pshow_+([0-9])/}

    # If the url contains the filename parameter then use that
    if [[ $FIXURL = *"filename="*  ]]; then
        FILENAME="${FIXURL/*filename=\"/}"
        FILENAME=$(echo "${FILENAME}" | cut -f1 -d'"')
        debug "From filename parameter = $FILENAME"
    fi

    case $FIXURL in
    # Fix MSNBC podcast names for audio feeds
     *msnbc*)
        [[ $FIRSTFILENAME =~ pd_.*|pdm_.*|vh-.*|a_zeit_.*|pdv_.* ]] && FILENAME=${BASH_REMATCH[0]}
    ;;
    # Only generic media.mp3 name but specific folder
    *media.acast.com*)
        FILENAME="${FOLDER_NAME}.mp3"
    ;;
    # They are all podcast.mp3 distinguished by fileId url param
    *scientificamerican*)
        FILENAME="sciam-${FIRSTFILENAME#*fileId=}.mp3"
    ;;
    esac

    # Remove parentheses in filenames
    FILENAME=${FILENAME//[()]}

    # Replace URL hex sequences in filename (like %20 for ' ' and %2B for '+')
    FILENAME=$(printf %b "${FILENAME//%/\\x}")

    # Replace spaces in filename with underscore
    FILENAME=${FILENAME// /_}

    IS_AUDIO=$(isAudio "$FILENAME")
    #debug "IS_AUDIO = $IS_AUDIO"
    if [[ "$IS_AUDIO" == "no" ]]; then
        FILENAME=$SUGGESTION
    fi
    FILENAME=$(echo "$FILENAME" | sed -e 's/[^A-Za-z0-9._-]//g') # sanitize
    debug "FINAL FILENAME = $FILENAME"
}

check_directory () {
    # Check to see if DATADIR exists and if not, create it
    if [ ! -e "$PODCASTDIR/$DATADIR" ]; then
        info "The directory $PODCASTDIR/$DATADIR for $FEED does not \
            exist.  Creating now..."
        mkdir -p "$PODCASTDIR/$DATADIR"
    fi
    return 0
}

function fetch_podcasts()
{
    # This is the main loop
    local LINE FEED DATADIR DLNUM COUNTER FILE URL FILENAME DLURL FILENAME_FEED

    # Read the subscriptions.conf file and wget any url not already in the podcast.log file:
    NEWDL=0
    while read -r LINE; do
        FEED=$(echo "$LINE" | cut -f1 -d ' ')
        DATADIR=$(echo "$LINE" | cut -f2 -d ' ')
        DLNUM=$(echo "$LINE" | cut -f3 -d ' ')
        COUNTER=0

        if [ "$DLNUM" = "all" ]; then
            echo "Checking ${FEED} -- get all episodes."
        elif [ "$DLNUM" = "none" ]; then
            echo "No downloads selected for $FEED."
            continue
        elif [ "$DLNUM" = "update" ]; then
            echo "Catching $FEED up in logs."
        else
            echo "Checking $FEED -- last $DLNUM episode(s)."
        fi

        FILE=$($WGET -q "$FEED" -O - | \
            $XSLTPROC "$PARSE_ENCLOSURE" - 2> /dev/null) || \
            FILE=$($WGET -q "$FEED" -O - | grep url= | \
            sed -n 's/.*url="\([^"]*\)".*/\1/p')

        if [[ -z "$FILE" ]]; then
            echo "ERROR: Cannot parse any episodes in ${FEED}"
            echo "ERROR: Cannot parse any episodes in ${FEED}" >> $SUMMARYLOG
            continue
        fi

        for URL in $FILE; do

            URL="${URL//&#39;/"'"}"
            debug "Base URL: ${URL}"

            # Skip non audio
            EXT=${URL: -4}
            if [[ "$EXT" == ".jpg" || "$EXT" == "jpeg" || "$EXT" == ".png" ||
                  "$EXT" == ".gif" || "$EXT" == ".php"  || "$EXT" == ".xml" ||
                  "$EXT" == "html"  || "$EXT" == "json" ]]; then
                info "Skipping non audio file $EXT"
                continue
            fi

            FILENAME_FEED=${URL##*/}
            FILENAME_FEED=${FILENAME_FEED%%\?*}

            if [ "$DLNUM" = "$COUNTER" ]; then
                break
            fi

            # Get the effective URL and extract file from there
            #DLURL=$($CURL -s -I -L -w "%{url_effective}" --url "$URL" | tail -n 1)
            DLURL=$($CURL -s -S -I -L -w "%{url_effective}" --url "$URL" -o /dev/null)
            DLURL=${DLURL//[$'\t\r\n ']} # trim DURL
            DLURL=${DLURL//&#39;/"'"}
            debug "Download URL: ${DLURL}"

            FILENAME=''
            fix_url "$DLURL" "$FILENAME_FEED"

            info "Found $FILENAME in feed "

            if ! grep -x "^$FILENAME$" "$PODLOG" > /dev/null; then

                info "but not in \$PODLOG. Proceeding."

                if [ "$DLNUM" = "update" ]; then
                    echo "$FILENAME" >> "$TEMPLOG"
                    info "Adding $FILENAME to \$PODLOG and continuing."
                    echo "$FILENAME added to \$PODLOG" >> $SUMMARYLOG
                    continue
                fi

                check_directory

                if [ ! -e "$PODCASTDIR/$DATADIR/$FILENAME" ]; then

                    echo "NEW: Fetching $FILENAME and saving in $DATADIR directory."

                    cd $TMPDIR || exit 107
                    RET=0
                    $WGET "$WGET_QUIET" -T $WGET_TIMEOUT -O "$FILENAME" "$DLURL"

                    FILESIZE=$(stat --printf="%s" "$FILENAME")
                    debug "FILESIZE: $FILESIZE"
                    if [[ $FILESIZE -eq 0 ]]; then
                        error "Failed to load ${DLURL}."
                        echo "---"
                        ((COUNTER=COUNTER+1))
                        continue
                    fi

                    ((NEWDL=NEWDL+1))
                    mv "$FILENAME" "$PODCASTDIR/$DATADIR/$FILENAME"
                    cd "$BASEDIR" || exit 109

                    echo "$FILENAME downloaded to $DATADIR" >> $SUMMARYLOG
                else
                    info "$FILENAME appears to already exist in $DATADIR directory. Skipping."
                fi

                echo "$FILENAME" >> "$TEMPLOG"
                echo "${PODCASTDIR}/${DATADIR}/${FILENAME}" >> $LATESTLOG
            else
                info "and in \$PODLOG. Skipping."
            fi
            ((COUNTER=COUNTER+1))
        done

        info "DONE. Continuing with next feed."
        info "---"

    done < "$TEMPRSSFILE"
}

function final_cleanup()
{
    # Delete temp files, create the log files, and clean up
    info "Cleaning up."

    cat "$PODLOG" >> "$TEMPLOG"
    sort "$TEMPLOG" | uniq > "$PODLOG"
    rm -f "$TEMPLOG"
    rm -f $TEMPRSSFILE

    info "Total downloads: $NEWDL"

    if [[ -s "$LATESTLOG" ]]; then
        cat "$LATESTLOG" > "$BASEDIR/latest.log"
    fi

    if $VERBOSE; then
        if [ -e $SUMMARYLOG ]; then
            echo
            echo "Summary:"
            cat $SUMMARYLOG
        fi
    fi
    rm -f $SUMMARYLOG

    # These next 2 lines were moved here so if the user kills the program with ctrl-C (see the trap
    # code, below), they will also cd to cwd before exiting.
    cd "$CWD" || return
    IFS="$OLDIFS"
}

function usage()
{
cat << EOF
$SCRIPT $VERSION
Usage: $SCRIPT [FLAGS] [OPTIONS] <arguments>
Flags
    -b, --backup-log            Create a date-stamped backup of the podcast.log file
    -f, --first-only            Override subscriptions.conf and download the first new episode
    -h, --help                  Display this help message
    -u, --update-only           Override subscriptions.conf and only update (mark downloaded)
    -v, --verbose               Display verbose messages and summary
        --version               Display version

Options
    -c, --config <filename>     Use a different config file other than subscriptions.conf.
    -d, --date <date>           Valid date string for date-based archiving. See man date.

subscriptions.conf is the standard configuration file. See the sample subscriptions.conf for
how this file is to be configured.

Default settings can be set at the top of the script in the 'USER CONFIGURATION' section.
The 'USER CONFIGURATION' section has additional settings that have no command line flags.

Reading $SCRIPT is recommended!

EOF
}

# THIS IS THE ACTUAL START OF SCRIPT
# Parse Options
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -b|--backup-log)
    PODLOG_BACKUP="1"
    shift # past argument
    ;;
    -c|--config)
    readonly RSSFILE="$2"
    shift # past argument
    shift # past value
    ;;
    -f|--first-only)
    readonly FIRST_ONLY=1
    shift # past argument
    ;;
    -u|--update-only)
    readonly UPDATE=1
    shift # past argument
    ;;
    -v|--verbose)
    readonly VERBOSE=true
    WGET_QUIET=""
    shift # past argument
    ;;
    -h|--help)
    usage
    exit 0
    shift # past argument
    ;;
    --version)
    echo "$VERSION"
    exit 0
    shift # past argument
    ;;
    *)  # unknown option
    echo "Unknown option $key"
    exit 255
    ;;
esac
done
# End of option parsing

shift $((OPTIND - 1))

# Trap ctrl-C's and other interrupts, clean up temp files, and exit cleanly.
# Thanks to mr.gaga for the report.
for sig in INT TERM HUP; do
    trap '
    echo
    echo signal $sig recived
    final_cleanup
    exit 0' $sig;
done

sanity_checks
fetch_podcasts
final_cleanup

exit 0
