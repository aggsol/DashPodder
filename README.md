# DashPodder

*Simple, hackable podcatcher written in Bash*

![Screenshot](https://gitlab.com/aggsol/DashPodder/raw/master/images/screenshot01.png)

## Install

Create a folder for the podcasts probably in your home. That folder is referenced
as `$BASEDIR`. Only three files are needed: dashpodder.sh, subscriptions.conf and
parse_enclosure.xsl. Copy those files there and edit the `USER CONFIGURATION`
section at the beginning of dashpodder.sh.

### Example
```sh
$ mkdir -p $HOME/dashpodder
$ cp -t $HOME/dashpodder dashpodder.sh subscriptions.conf parse_enclosure.xsl
```

Edit subscriptions.conf to add you podcast subscriptions by adding feed URLs.
See subscriptions.conf for details and examples.

## Usage

Call dashpodder.sh manually or with a cron job to update and download new podcasts.
For desktop usage you can also use anacron. There is a cron wrapper example in `/tools`.
Integration into the Gnome Shell is possible with the Argos extension.

## Requirements

* Bash >= 4.0
* wget
* curl
* xsltproc

### Gnome Shell Integration

1. Install [Argos](https://extensions.gnome.org/extension/1176/argos/)
2. Copy `tools/dashpodder.r.1h.sh` to `~/.config/argos`
3. Edit user configuration in `dashpodder.r.1h.sh`

## Changes

### v1.1.3-alpha
- Fix #2
- Check for single quote HTML entities in URLs
- Check file size after download
- Clean `$TMPDIR` before downloads

### v1.1.2 (2018-04-23)
- Sanitize filenames
- Improved Argos plugin error handling
- Improved filename detection and error handling
- Fix shellcheck warnings

## History

This is a fork of [mashpodder](https://github.com/chessgriffin/mashpodder) that
is no longer maintained. It was based on [BashPodder](http://lincgeek.org/bashpodder/).
Someone already named a fork [smashpodder](https://github.com/agilbertson1977/smashpodder)
hence the name DashPodder :-)

## License

[BSD-2-Clause](https://opensource.org/licenses/BSD-2-Clause)
